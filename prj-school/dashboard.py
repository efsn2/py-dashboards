import dash
import dash_bootstrap_components as dbc
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State
from textwrap import wrap
import pandas as pd
import numpy as np
from utils import *

import plotly.express as px
import plotly.graph_objects as go

# dataframe used in this project
df = pd.read_csv("amazon.csv")
url = pd.read_csv("bookdata.csv")
df = pd.merge(df, url, how="left", on="Name")
links = [df["URL"][3], df["URL"][3], df["URL"][3], df["URL"][3]]

# the style arguments for the sidebar.
SIDEBAR_STYLE = {
    "position": "fixed",
    "top": 0,
    "left": 0,
    "bottom": 0,
    "width": "20%",
    "padding": "20px 10px",
    "background-color": "#f8f9fa",
}

# the style arguments for the main content page.
CONTENT_STYLE = {"margin-left": "25%", "margin-right": "5%", "padding": "20px 10p"}

TEXT_STYLE = {"textAlign": "center", "color": "#191970"}

CARD_TEXT_STYLE = {"textAlign": "center", "color": "#0074D9"}

controls = dbc.FormGroup(
    [
        html.P("Gênero do Livro", style={"textAlign": "center"}),
        dbc.Card(
            [
                dbc.Checklist(
                    id="check_list",
                    options=[
                        {"label": "Ficção", "value": "Fiction"},
                        {"label": "Não Ficção", "value": "Non Fiction"},
                    ],
                    value=["Fiction", "Non Fiction"],
                    style={"textAlign": "center"},
                    inline=True,
                )
            ]
        ),
        html.Br(),
        html.P("Selecione intervalo de comparação", style={"textAlign": "center"}),
        dcc.RangeSlider(
            min=2009,
            max=2019,
            id="intervalo",
            step=None,
            marks={str(year): str(year) for year in df["Year"].unique()},
            value=[df["Year"].min(), df["Year"].max()],
        ),
        html.Br(),
    ]
)

sidebar = html.Div(
    [html.H2("Parâmetros", style=TEXT_STYLE), html.Hr(), controls],
    style=SIDEBAR_STYLE,
)


card_book1 = [
    dbc.Card(
        [
            dbc.CardImg(
                top=True,
                bottom=False,
                id="card1-img",
            ),
            dbc.CardBody(
                [
                    html.Div(
                        html.Img(
                            src="/assets/star.png",
                            style={"height": "10%", "width": "10%"},
                        ),
                        style={"textAlign": "center"},
                    ),
                    html.H4(
                        {},
                        className="card-title",
                        id="card1",
                        style={"textAlign": "center"},
                    ),
                ]
            ),
        ],
        color="light",  # https://bootswatch.com/default/ for more card colors
        inverse=False,  # change color of text (black or white)
        outline=False,  # True = remove the block colors from the background and header
        id="card1",
    )
]

card_book2 = [
    dbc.Card(
        [
            dbc.CardImg(
                src=links[1],
                top=True,
                bottom=False,
                id="card2-img",
            ),
            dbc.CardBody(
                [
                    html.Div(
                        html.Img(
                            src="/assets/star.png",
                            style={"height": "10%", "width": "10%"},
                        ),
                        style={"textAlign": "center"},
                    ),
                    html.H4(
                        {},
                        className="card-title",
                        id="card2",
                        style={"textAlign": "center"},
                    ),
                ]
            ),
        ],
        color="light",  # https://bootswatch.com/default/ for more card colors
        inverse=False,  # change color of text (black or white)
        outline=False,  # True = remove the block colors from the background and header
    )
]

card_book3 = [
    dbc.Card(
        [
            dbc.CardImg(
                src=links[2],
                top=True,
                bottom=False,
                id="card3-img",
            ),
            dbc.CardBody(
                [
                    html.Div(
                        html.Img(
                            src="/assets/star.png",
                            style={"height": "10%", "width": "10%"},
                        ),
                        style={"textAlign": "center"},
                    ),
                    html.H4(
                        {},
                        className="card-title",
                        id="card3",
                        style={"textAlign": "center"},
                    ),
                ]
            ),
        ],
        color="light",  # https://bootswatch.com/default/ for more card colors
        inverse=False,  # change color of text (black or white)
        outline=False,  # True = remove the block colors from the background and header
    )
]

card_book4 = [
    dbc.Card(
        [
            dbc.CardImg(
                src=links[3],
                top=True,
                bottom=False,
                id="card4-img",
            ),
            dbc.CardBody(
                [
                    html.Div(
                        html.Img(
                            src="/assets/star.png",
                            style={"height": "10%", "width": "10%"},
                        ),
                        style={"textAlign": "center"},
                    ),
                    html.H4(
                        {},
                        className="card-title",
                        id="card4",
                        style={"textAlign": "center"},
                    ),
                ]
            ),
        ],
        color="light",  # https://bootswatch.com/default/ for more card colors
        inverse=False,  # change color of text (black or white)
        outline=False,  # True = remove the block colors from the background and header
    )
]


# Card components
cards = [card_book1, card_book2, card_book3, card_book4]

# Primeira Linha  === CARDS
content_cards = dbc.Row([dbc.Col(card) for card in cards])

# Segunda LInha === Graficos Diversos
content_second_row = dbc.Col(dcc.Graph(id="graph_1"))

# grafico dos precos
content_prices = dbc.Col(dcc.Graph(id="prices"))

# Gráfico de anderson
content_anderson = dbc.Col(dcc.Graph(id="graph_2"))

# Terceira linha === markdown
content_fifth_row = dbc.Row(
    dcc.Markdown("""Pós CEASR School - Análise e Engenharia de Dados - Grupo III"""),
)


select_page = html.Div(
    [
        dcc.Tabs(
            id="tabs-example-graph",
            value="tab-1-example-graph",
            children=[
                dcc.Tab(label="Livros Populares", value="tab-1-example-graph"),
                dcc.Tab(label="Autores Mais Populares", value="tab-2-example-graph"),
            ],
        ),
        html.Div(id="tabs-content-example-graph"),
    ]
)

content_page1 = html.Div(
    [
        html.H2("Amazon - Top Sellers Dashboard", style=TEXT_STYLE),
        html.Hr(),
        content_second_row,
        html.Hr(),
        html.H4("Comparação dos Preços (em US$)", style=TEXT_STYLE),
        content_prices,
        # FIXME: figura ok
        html.H4("Os livros mais bem avaliados", style=TEXT_STYLE),
        content_cards,
        html.Hr(),
        content_fifth_row,
    ],
    style=CONTENT_STYLE,
)

content_page2 = html.Div(
    [
        html.H2("Amazon - Top Authors", style=TEXT_STYLE),
        html.Hr(),
        html.H3("O gráfico dos autores mais populares", style=TEXT_STYLE),
        html.Hr(),
        content_second_row,
        content_anderson,
    ],
    style=CONTENT_STYLE,
)

app = dash.Dash(external_stylesheets=[dbc.themes.BOOTSTRAP])
app.layout = html.Div([sidebar, select_page])


@app.callback(
    Output("tabs-content-example-graph", "children"),
    Input("tabs-example-graph", "value"),
)
def render_content(tab):
    if tab == "tab-1-example-graph":
        return content_page1
    elif tab == "tab-2-example-graph":
        return content_page2


def insert_linebreak(string, lengLabel=48):
    return "\n".join(string[i : i + lengLabel] for i in xrange(0, len(string), every))


# primeira callback -- dataframe filtering!
@app.callback(
    Output("graph_1", "figure"),
    [Input("check_list", "value")],
    [Input("intervalo", "value")],
    [Input("tabs-example-graph", "value")],
)
def update_graph_1(options_chosen, yearr, filter_pr):
    # filtra o genero
    dff = df.loc[df["Genre"].isin(options_chosen)]
    # filtra o intervalo de ano
    dff = dff.loc[(dff["Year"] >= yearr[0]) & (dff["Year"] <= yearr[1])]
    df_g1 = dff.sort_values(by="Reviews", ascending=False).head(10)
    df_g2 = df_g1.groupby("Name", as_index=False)[["Reviews"]].sum()
    df_g2.columns = ["Name", "Total"]
    df_g1 = df_g1.merge(df_g2, on="Name")
    df_g1 = df_g1.sort_values(by="Total", ascending=False)
    df_g1["Year"] = df_g1["Year"].astype(str)
    # FIXME: melhorar a visualizacao das strings!
    # df_g1["Name"] = ['\n'.join(wrap(1,12)) for l in df_g1["Name"]]
    # df_g1['Name'] = ['\n'.join(wrap(x, 12)) for x in  df_g1['Name']]
    if filter_pr == "tab-1-example-graph":
        fig = px.bar(
            data_frame=df_g1,
            x="Reviews",
            y="Name",
            color="Year",
            color_discrete_sequence=px.colors.sequential.Blues[
                3 : 4 + df_g1["Name"].nunique()
            ],
        )
        fig.update_layout(
            plot_bgcolor="rgba(0,0,0,0)",
            title="Livros Mais Populares",
            xaxis_title="Número de Reviews",
            yaxis_title="",
            legend_title="Ano",
            font=dict(family="Courier New, monospace", size=12, color="RebeccaPurple"),
        )
        return fig
    elif filter_pr == "tab-2-example-graph":
        fig = px.bar(
            data_frame=df_g1,
            x="Reviews",
            y="Author"[:9],
            title="Autores Mais Populares",
            color="Year",
            color_discrete_sequence=px.colors.sequential.Blues[
                3 : 4 + df_g1["Author"].nunique()
            ],
        )
        fig.update_layout(
            plot_bgcolor="rgba(0,0,0,0)",
            title="Autores Mais Populares",
            xaxis_title="Número de Reviews",
            yaxis_title="",
            legend_title="Ano",
            font=dict(family="Calibri", size=14, color="RebeccaPurple"),
        )
        return fig


# primeira callback -- dataframe filtering!
@app.callback(
    Output("prices", "figure"),
    [Input("check_list", "value")],
    [Input("intervalo", "value")],
    [Input("tabs-example-graph", "value")],
)
def update_prices(options_chosen, yearr, filter_pr):
    # filtra o genero
    dff = df.loc[df["Genre"].isin(options_chosen)]
    # filtra o intervalo de ano
    dff = dff.loc[(dff["Year"] >= yearr[0]) & (dff["Year"] <= yearr[1])]
    price_max = dff["Price"].max()
    price_avg = dff["Price"].mean()
    dff = dff.loc[dff['Price'] > 0]
    price_min = dff['Price'].min()
    
    print(f"O preço médio é {price_avg} || maximo {price_max} || menor {price_min}")
    prices = go.Figure()

    prices.add_trace(
        go.Indicator(
            mode="number+delta",
            value=price_max,
            title={
                "text": "Maior Preço<br><span style='font-size:0.8em;color:gray'></span><br><span style='font-size:0.8em;color:gray'></span>"
            },
            domain={'row': 0, 'column': 0},
            delta={"reference": price_avg, "relative": True, "position": "top", "valueformat": ".2f"},
        )
    )

    prices.add_trace(
        go.Indicator(
            mode="number",
            value=price_avg,
            title={
                "text": "Preço Médio<br><span style='font-size:0.8em;color:gray'></span><br><span style='font-size:0.8em;color:gray'></span>"
            },
            domain={'row': 0, 'column': 1},
        )
    )

    prices.add_trace(
        go.Indicator(
            mode="number+delta",
            value=price_min,
            title={
                "text": "Menor Preço<br><span style='font-size:0.8em;color:gray'></span><br><span style='font-size:0.8em;color:gray'></span>"
            },
            delta={"reference": price_avg, "relative": True, "valueformat": ".2f"},
            domain={'row': 0, 'column': 2},
        )
    )

    prices.update_layout(grid = {'rows': 1, 'columns': 3, 'pattern': "independent"})

    return prices


# segundo gráfico
@app.callback(
    Output("graph_2", "figure"),
    [Input("check_list", "value")],
    [Input("intervalo", "value")],
)
def update_graph_2(options_chosen, yearr):
    # filtra o genero
    dff = df.loc[df["Genre"].isin(options_chosen)]
    # filtra o intervalo de ano
    dff = dff.loc[(dff["Year"] >= yearr[0]) & (dff["Year"] <= yearr[1])]
    dff = dff.groupby(["Author"], as_index=False)[["Reviews"]].sum()
    dff.sort_values("Reviews", ascending=False, inplace=True)
    dff = dff.head(5)
    # formato link wikipedia   https://pt.wikipedia.org/wiki/Suzanne_Collins
    dff["url"] = "https://pt.wikipedia.org/wiki/" + dff["Author"]

    # gráfico
    fig2 = px.bar(dff, y="Reviews", x="Author")
    for r in dff.iterrows():
        fig2.add_annotation(
            {
                "x": r[1]["Author"],
                "y": r[1]["Reviews"],
                "text": f"""<a href="{r[1]["url"]}" target="_blank">{r[1]["Author"]}</a>""",
            }
        )
        fig2.update_layout(
            plot_bgcolor="rgba(0,0,0,0)",
            title="Os autores mais populares a partir dos livros vendidos",
            yaxis_title="Quantidade de Reviews",
            font=dict(family="Calibri", size=14, color="RebeccaPurple"),
        )

    return fig2


@app.callback(
    [
        Output("card1-img", "src"),
        Output("card2-img", "src"),
        Output("card3-img", "src"),
        Output("card4-img", "src"),
        Output("card1", "children"),
        Output("card2", "children"),
        Output("card3", "children"),
        Output("card4", "children"),
    ],
    [Input("check_list", "value")],
    [Input("intervalo", "value")],
    [Input("tabs-example-graph", "value")],
)
def update_cards(options_chosen, yearr, filter_pr):
    # filtra o genero
    dff = df.loc[df["Genre"].isin(options_chosen)]
    dff = dff.drop_duplicates(subset=["Name"])
    dff = dff.sort_values(by="Reviews", ascending=False).head(10)
    # filtra o intervalo de ano
    dff = dff.loc[(dff["Year"] >= yearr[0]) & (dff["Year"] <= yearr[1])]
    df_g1 = dff.sort_values(by="User Rating", ascending=False).head(8)
    # return names!
    ndf = df_g1.nlargest(4, ["User Rating"])
    lista = []
    grade = []
    for ind in ndf.index:
        lista.append(df["URL"][ind])
        grade.append(df["User Rating"][ind])
    card1 = lista[0]
    card2 = lista[1]
    card3 = lista[2]
    card4 = lista[3]
    card_gr1 = grade[0]
    card_gr2 = grade[1]
    card_gr3 = grade[2]
    card_gr4 = grade[3]
    return card1, card2, card3, card4, card_gr1, card_gr2, card_gr3, card_gr4


if __name__ == "__main__":
    app.run_server(port="8085")

# FIXME:
# Links uteis
# paletas de cores
# https://plotly.com/python/builtin-colorscales/
# https://plotly.com/python/discrete-color/
