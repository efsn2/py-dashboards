# Py Dashboards

Dashboard examples and templates to be used in data science projects

```
    python -m venv venv
    source venv/bin/activate
    pip install -r requirements.txt
```


## References

- [Plotly Graphing Library](https://plotly.com/graphing-libraries/)

- [Dash Gallery Templates](https://dash.gallery/Portal/)

- [Dash Sample Apps Github](https://github.com/plotly/dash-sample-apps/tree/main/apps)

- [Dash Documentation](https://dash.plotly.com/introduction)

- [Analytics Template](https://medium.com/analytics-vidhya/python-dash-data-visualization-dashboard-template-6a5bff3c2b76)

- [YouTube video: introduction to Dash Plotly](https://www.youtube.com/watch?v=hSPmj7mK6ng&ab_channel=CharmingData)