import dash
import dash_bootstrap_components as dbc
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
import pandas as pd
import plotly.express as px

df = pd.read_csv("newdf.csv")
app = dash.Dash(__name__, external_stylesheets=[dbc.themes.BOOTSTRAP])

# plotly para as figuras
fig1 = px.histogram(df, x="age", title="Histograma da Idade")
fig2 = px.box(df, x="cardio", y="ap_lo")

# definicacao das figuras usandi plotly
# Card components
cards = [
    # CARD (1) - Acuracia, fundo branco
    dbc.Card(
        [
            html.H2(f"{92.2}%", className="card-title"),
            html.P("Model Training Accuracy", className="card-text"),
        ],
        body=True,
        color="light",
    ),
    # CARD (2) - Acuracia, fundo preto
    dbc.Card(
        [
            html.H2(f"{100}%", className="card-title"),
            html.P("Model Test Accuracy", className="card-text"),
        ],
        body=True,
        color="dark",
        inverse=True,
    ),
    # CARD (3) - train/test split, fundo azul
    dbc.Card(
        [
            html.H2(f"532 / 999", className="card-title"),
            html.P("Train / Test Split", className="card-text"),
        ],
        body=True,
        color="primary",
        inverse=True,
    ),
]

# Markdown ilustrativo para colocar no dashboard
mkd = dcc.Markdown('''[Dash User Guide](/)''')

graphs = [dcc.Graph(id="grafico1", figure=fig1), dcc.Graph(id="grafico2", figure=fig2)]

# Elementos gráficos do dash board
app.layout = dbc.Container(
    [
        html.H1("Dash Template - Eron2", style={"text-align": "center"}),
        html.Hr(),
        dbc.Row([dbc.Col(card) for card in cards]),
        html.Br(),
        dbc.Row([dbc.Col(graph) for graph in graphs]),
        html.Br(),
        dbc.Row(mkd),
    ],
    fluid=False,
)


if __name__ == "__main__":
    # TODO: note que tem a opcao de colocar true ou false para debug
    app.run_server(debug=False)
