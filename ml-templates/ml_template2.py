import dash
import dash_bootstrap_components as dbc
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State

import plotly.express as px

# the style arguments for the sidebar.
SIDEBAR_STYLE = {
    "position": "fixed",
    "top": 0,
    "left": 0,
    "bottom": 0,
    "width": "20%",
    "padding": "20px 10px",
    "background-color": "#f8f9fa",
}

# the style arguments for the main content page.
CONTENT_STYLE = {"margin-left": "25%", "margin-right": "5%", "padding": "20px 10p"}

TEXT_STYLE = {"textAlign": "center", "color": "#191970"}

CARD_TEXT_STYLE = {"textAlign": "center", "color": "#0074D9"}

controls = dbc.FormGroup(
    [
        html.P("Dropdown", style={"textAlign": "center"}),
        dcc.Dropdown(
            id="dropdown",
            options=[
                {"label": "Value One", "value": "value1"},
                {"label": "Value Two", "value": "value2"},
                {"label": "Value Three", "value": "value3"},
            ],
            value=["value1"],  # default value
            multi=True,
        ),
        html.Br(),
        html.P("Range Slider", style={"textAlign": "center"}),
        dcc.RangeSlider(id="range_slider", min=0, max=20, step=0.5, value=[5, 15]),
        html.P("Check Box", style={"textAlign": "center"}),
        dbc.Card(
            [
                dbc.Checklist(
                    id="check_list",
                    options=[
                        {"label": "Value One", "value": "value1"},
                        {"label": "Value Two", "value": "value2"},
                        {"label": "Value Three", "value": "value3"},
                    ],
                    value=["value1", "value2"],
                    inline=True,
                )
            ]
        ),
        html.Br(),
        html.P("Radio Items", style={"textAlign": "center"}),
        dbc.Card(
            [
                dbc.RadioItems(
                    id="radio_items",
                    options=[
                        {"label": "Value One", "value": "value1"},
                        {"label": "Value Two", "value": "value2"},
                        {"label": "Value Three", "value": "value3"},
                    ],
                    value="value1",
                    style={"margin": "auto"},
                )
            ]
        ),
        html.Br(),
        dbc.Button(
            id="submit_button",
            n_clicks=0,
            children="Submit",
            color="primary",
            block=True,
        ),
    ]
)

logo = html.Div(
    [
        html.Img(src="dash-logo.png", alt="MIT Logo", height="30px"),
    ],
)

sidebar = html.Div(
    [html.H2("Parâmetros", style=TEXT_STYLE), html.Hr(), controls],
    style=SIDEBAR_STYLE,
)

# Card components
cards = [
    # CARD (1) - Acuracia, fundo branco
    dbc.Card(
        [
            html.H2(f"{92.2}%", className="card-title"),
            html.P("Model Training Accuracy", className="card-text"),
        ],
        body=True,
        color="light",
    ),
    # CARD (2) - Acuracia, fundo preto
    dbc.Card(
        [
            html.H2(f"{100}%", className="card-title"),
            html.P("Model Test Accuracy", className="card-text"),
        ],
        body=True,
        color="dark",
        inverse=True,
    ),
    # CARD (3) - train/test split, fundo azul
    dbc.Card(
        [
            html.H2(f"532 / 999", className="card-title"),
            html.P("Train / Test Split", className="card-text"),
        ],
        body=True,
        color="primary",
        inverse=True,
    ),
]

# Primeira Linha  === CARDS
content_first_row = dbc.Row([dbc.Col(card) for card in cards])

# Segunda LInha === Graficos Diversos
content_second_row = dbc.Row(
    [
        dbc.Col(dcc.Graph(id="graph_1"), md=4),
        dbc.Col(dcc.Graph(id="graph_2"), md=4),
        dbc.Col(dcc.Graph(id="graph_3"), md=4),
    ]
)

# Terceira Linha
content_third_row = dbc.Row(
    [
        dbc.Col(
            dcc.Graph(id="graph_4"),
            md=12,
        )
    ]
)

content_fourth_row = dbc.Row(
    [dbc.Col(dcc.Graph(id="graph_5"), md=6), dbc.Col(dcc.Graph(id="graph_6"), md=6)]
)

content_fifth_row = dbc.Row(dcc.Markdown('''[Dash User Guide](/)'''))

content = html.Div(
    [
        html.H2("Eron3 - Template", style=TEXT_STYLE),
        html.Hr(),
        content_first_row,
        content_second_row,
        content_third_row,
        content_fourth_row,
        content_fifth_row
    ],
    style=CONTENT_STYLE,
)

app = dash.Dash(external_stylesheets=[dbc.themes.BOOTSTRAP])
app.layout = html.Div([logo, sidebar, content])


@app.callback(
    Output("graph_1", "figure"),
    [Input("submit_button", "n_clicks")],
    [
        State("dropdown", "value"),
        State("range_slider", "value"),
        State("check_list", "value"),
        State("radio_items", "value"),
    ],
)
def update_graph_1(
    n_clicks, dropdown_value, range_slider_value, check_list_value, radio_items_value
):
    print(n_clicks)
    print(dropdown_value)
    print(range_slider_value)
    print(check_list_value)
    print(radio_items_value)
    fig = {"data": [{"x": [1, 2, 3], "y": [3, 4, 5]}]}
    return fig


@app.callback(
    Output("graph_2", "figure"),
    [Input("submit_button", "n_clicks")],
    [
        State("dropdown", "value"),
        State("range_slider", "value"),
        State("check_list", "value"),
        State("radio_items", "value"),
    ],
)
def update_graph_2(
    n_clicks, dropdown_value, range_slider_value, check_list_value, radio_items_value
):
    print(n_clicks)
    print(dropdown_value)
    print(range_slider_value)
    print(check_list_value)
    print(radio_items_value)
    fig = {"data": [{"x": [1, 2, 3], "y": [3, 4, 5], "type": "bar"}]}
    return fig


@app.callback(
    Output("graph_3", "figure"),
    [Input("submit_button", "n_clicks")],
    [
        State("dropdown", "value"),
        State("range_slider", "value"),
        State("check_list", "value"),
        State("radio_items", "value"),
    ],
)
def update_graph_3(
    n_clicks, dropdown_value, range_slider_value, check_list_value, radio_items_value
):
    print(n_clicks)
    print(dropdown_value)
    print(range_slider_value)
    print(check_list_value)
    print(radio_items_value)
    df = px.data.iris()
    fig = px.density_contour(df, x="sepal_width", y="sepal_length")
    return fig


@app.callback(
    Output("graph_4", "figure"),
    [Input("submit_button", "n_clicks")],
    [
        State("dropdown", "value"),
        State("range_slider", "value"),
        State("check_list", "value"),
        State("radio_items", "value"),
    ],
)
def update_graph_4(
    n_clicks, dropdown_value, range_slider_value, check_list_value, radio_items_value
):
    print(n_clicks)
    print(dropdown_value)
    print(range_slider_value)
    print(check_list_value)
    print(radio_items_value)  # Sample data and figure
    df = px.data.gapminder().query("year==2007")
    fig = px.scatter_geo(
        df,
        locations="iso_alpha",
        color="continent",
        hover_name="country",
        size="pop",
        projection="natural earth",
    )
    fig.update_layout({"height": 600})
    return fig


@app.callback(
    Output("graph_5", "figure"),
    [Input("submit_button", "n_clicks")],
    [
        State("dropdown", "value"),
        State("range_slider", "value"),
        State("check_list", "value"),
        State("radio_items", "value"),
    ],
)
def update_graph_5(
    n_clicks, dropdown_value, range_slider_value, check_list_value, radio_items_value
):
    print(n_clicks)
    print(dropdown_value)
    print(range_slider_value)
    print(check_list_value)
    print(radio_items_value)  # Sample data and figure
    df = px.data.iris()
    fig = px.scatter(df, x="sepal_width", y="sepal_length")
    return fig


@app.callback(
    Output("graph_6", "figure"),
    [Input("submit_button", "n_clicks")],
    [
        State("dropdown", "value"),
        State("range_slider", "value"),
        State("check_list", "value"),
        State("radio_items", "value"),
    ],
)
def update_graph_6(
    n_clicks, dropdown_value, range_slider_value, check_list_value, radio_items_value
):
    print(n_clicks)
    print(dropdown_value)
    print(range_slider_value)
    print(check_list_value)
    print(radio_items_value)  # Sample data and figure
    df = px.data.tips()
    fig = px.bar(df, x="total_bill", y="day", orientation="h")
    return fig


@app.callback(
    Output("card_title_1", "children"),
    [Input("submit_button", "n_clicks")],
    [
        State("dropdown", "value"),
        State("range_slider", "value"),
        State("check_list", "value"),
        State("radio_items", "value"),
    ],
)
def update_card_title_1(
    n_clicks, dropdown_value, range_slider_value, check_list_value, radio_items_value
):
    print(n_clicks)
    print(dropdown_value)
    print(range_slider_value)
    print(check_list_value)
    print(radio_items_value)  # Sample data and figure
    return "Card Tile 1 change by call back"


@app.callback(
    Output("card_text_1", "children"),
    [Input("submit_button", "n_clicks")],
    [
        State("dropdown", "value"),
        State("range_slider", "value"),
        State("check_list", "value"),
        State("radio_items", "value"),
    ],
)
def update_card_text_1(
    n_clicks, dropdown_value, range_slider_value, check_list_value, radio_items_value
):
    print(n_clicks)
    print(dropdown_value)
    print(range_slider_value)
    print(check_list_value)
    print(radio_items_value)  # Sample data and figure
    return "Card text change by call back"


if __name__ == "__main__":
    app.run_server(port="8085")
